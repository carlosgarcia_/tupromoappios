import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable } from 'rxjs/Observable';

/*
  Generated class for the PromoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PromoServiceProvider {
  private url: string = "http://cupon.ignisoftware.com/api/rest/";

  constructor(public http: Http) {
    this.getHome();
  }

  getHome(){
    return this.http.get(this.url+'home')
    .map(this.mapPromos)
    .catch(this.catchError);
  }

   private mapPromos(res: Response){
   console.log(res.json());
    return res.json();
   }

   private catchError(error: Response | any){
      console.log(error);
      return Observable.throw(error.json().error || "Server Error");
   }

}
