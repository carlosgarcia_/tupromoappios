import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PromoServiceProvider } from '../../providers/promo-service/promo-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
elite  = [];
goldens = [];
promos = [];

url = 'http://cupon.ignisoftware.com/';

  constructor(public navCtrl: NavController, private promoService: PromoServiceProvider) {
    this.getElite();
    this.getGold();
    this.getPromos();
  }

  getElite(){
    this.promoService.getHome().subscribe(data => this.elite = data.elite);
  }

  getGold(){
    this.promoService.getHome().subscribe(data => this.goldens = data.goldPromos);
  }
  getPromos(){
    this.promoService.getHome().subscribe(data => this.promos = data.promos);
  }

  searchPromos(value: string){
    if (value != ''){
      //this.navCtrl.setRoot(HomePage);
    }
  }

  getBusiness(value: string){
    console.log(value);
  }

}
